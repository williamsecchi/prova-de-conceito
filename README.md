# Prova de Conceito - IA

Esse projeto foi desenvolvido para a disciplina de Inteligência Artificial do curso de Ciência da
Computação da Universidade Regional Integrada do Alto Uruguai e das Missões.

Sob nenhuma hipótese deve ser utilizado em produção, pois não foi desenvolvido com tal intuito.

## Pontos de atenção

Tendo em vista que o projeto foi desenvolvido para fins acadêmicos, alguns pontos devem ser levados
em consideração:

- Não implementação de nenhuma camada de segurança;
- Não implementação de nenhuma camada de autenticação;
- Não implementação de nenhuma camada de autorização;
- Não implementação de nenhuma camada de validação;
- Não implementação de nenhuma camada de tratamento de erros;
- Não implementação de nenhuma camada de tratamento de exceções;
- Não implementação de nenhuma camada de tratamento de logs;
- Não implementação de nenhuma camada de tratamento de cache;
- Não implementação de nenhuma camada de tratamento de performance;
- Não implementação de nenhuma camada de tratamento de concorrência;
- Não implementação de um padrão de projeto;
- Não implementação de um padrão de arquitetura;

## Como executar

Para executar o projeto é necessário a instalação do [Flutter](https://flutter.dev/docs/get-started/install).
Além disso é necessário uma chave de API do Google GCP e também da OPEN AI. 

Quando obter as chaves, basta adicionar as mesmas via `--dart-define` no momento da execução do projeto.

As chaves são, respectivamente: 
 - OPEN_AI_API_KEY: chave da OPEN AI
 - GOOGLE_API_KEY: chave do Google GCP